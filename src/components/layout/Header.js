import React, { Component } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import logo from '../../assets/logo192.png';
import "./Header.css";

export class Header extends Component {
    render() {
        return (
            <>
                <Navbar bg="dark" variant="dark">
                    <Container>
                    <Navbar.Brand href="/">
                        <img src={logo} alt="App" className="ImgStyle"/>
                    </Navbar.Brand>
                    <Nav className="me-auto">
                    <Nav.Link href="/">Login</Nav.Link>
                    <Nav.Link href="/signup">Signup</Nav.Link>
                    </Nav>
                    </Container>
                </Navbar>
            </>
        )
    }
}

export default Header
