import React, { Component } from 'react';
import { Form , Button, InputGroup} from 'react-bootstrap';
import { FaRegEye, FaRegEyeSlash} from 'react-icons/fa';
import './Login.css';

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            emailError: '',
            passwordError: '',
            showPassword: false
        };
    }

    handleEmailChange = (event) => {
        let email = event.target.value;
        this.setState({email});

        email = email.trim();
        let emailValidator=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //eslint-disable-line

        if(email === '') {
            this.setState({
                emailError: 'Email is required'
            });
        } else if(!email.match(emailValidator)) {
            this.setState({
                emailError: 'Enter a valid email'
            });
        } else {
            this.setState({
                emailError: ''
            });
        }
    }

    handlePasswordChange = (event) => {
        let password = event.target.value;
        this.setState({password});

        let passwordValidator = /(?=^.{6,}$)(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^A-Za-z0-9]).*/;

        if(this.state.email === '') {
            this.setState({
                emailError: 'Email is required'
            })
        }

        if(password === '') {
            this.setState({
                passwordError: 'Password is required'
            });
        } else if(!password.match(passwordValidator)) {
            this.setState({
                passwordError: 'Enter a valid password'
            });
        } else {
            this.setState({
                passwordError: ''
            });
        }

    }

    isFormDirty = () => {
        let email = this.state.email;
        let password = this.state.password;
       
        let emailError = this.state.emailError;
        let passwordError = this.state.passwordError;

        if(email !== '' && password !== '' ) {
            return true;
        } 
        if(email === '' && password === '' &&  emailError === '' && passwordError === '' ){
            this.setState({
                emailError: 'Email is required',
                passwordError: 'Password is required',
            });
            return false;
        } 
        return false;
    }

    validateForm = () => {
        if(this.isFormDirty()) {
            let emailError = this.state.emailError;
            let passwordError = this.state.passwordError;

            if(emailError === '' && passwordError === '') {
                return true;
            } 
        }
        return false;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if(this.validateForm()) {
            let loginData = {
                email: this.state.email.trim(),
                password: this.state.password
            }
            this.props.handleLoginData(loginData);
        } 
    }

    handleShowPassword = () => {
        console.log(this.state.showPassword);
        this.setState({
            showPassword: !this.state.showPassword
        })
    }


    render() {
        return (
            <Form onSubmit={this.handleSubmit} className="Loginform">
                <h1 className="Heading">Login</h1>
                <Form.Group className="mb-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={this.state.email} 
                        onChange={this.handleEmailChange}
                        style={this.state.emailError !== '' ? ErrorStyle : (this.state.email !== '' ? SuccessStyle : null )}/>
                    <Form.Text style={{color: "red"}}>{this.state.emailError}</Form.Text>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <InputGroup className="mb-3">
                        <Form.Control 
                            type={this.state.showPassword ? "text" : "password"} 
                            value={this.state.password} 
                            onChange={this.handlePasswordChange}
                            style={this.state.passwordError !== '' ? ErrorStyle : (this.state.password !== '' ? SuccessStyle : null )}/>
                        <Button variant="secondary" id="button-addon2"  onClick={this.handleShowPassword}>
                            {this.state.showPassword ? <FaRegEyeSlash /> :  <FaRegEye />  }
                        </Button>
                    </InputGroup>
                    <div className="Col">
                        <Form.Text style={{color: "red"}}>{this.state.passwordError}</Form.Text>
                    </div>
                </Form.Group>
                <Button type="submit">Submit</Button>
            </Form>
        )
    }
}

const ErrorStyle = {
    border: "1px solid red"
}

const SuccessStyle = {
    border: "1.5px solid lightgreen"
}

export default Login
