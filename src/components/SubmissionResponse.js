import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

export class SubmissionResponse extends Component {
    render() {
        let status = true;
        let msg = "Your profile is successfully created";
        if(this.props.signupError !== "") {
            msg = this.props.signupError;
            status = false;
        }
        return (
            <div>
                {
                    !status && 
                    <Card style={{ width: '18rem' }}>
                        <Card.Body>
                            <Card.Title style={{color: "red"}}>Error</Card.Title>
                            <Card.Text>{msg}</Card.Text>
                            <Card.Link href="/signup">Singup</Card.Link>
                            <Card.Link href="/">Login</Card.Link>
                        </Card.Body>
                    </Card>
                }
                {
                    status && 
                    <Card style={{ width: '18rem', textAlign: 'center' }}>
                        <Card.Body>
                            <Card.Title style={{color: "green"}}>{msg}</Card.Title>
                            <Card.Link href="/">Login</Card.Link>
                        </Card.Body>
                    </Card> 
                }
            </div>
        )
    }
}

export default SubmissionResponse
