import React, { Component } from 'react';
import { Form , Button, InputGroup} from 'react-bootstrap';
import { FaRegEye, FaRegEyeSlash} from 'react-icons/fa';
import './Signup.css';

export class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            nameError: '',
            emailError: '',
            passwordError: '',
            confirmPasswordError: '',
            showPassword: false,
            showConfirmPassword: false
        };
    }

    handleNameChange = (event) => {
        let name = event.target.value;
        this.setState({name});
        name = name.trim();
        if(name === '') {
            this.setState({
                nameError: 'Name is required'
            });
        } else if(!name.match(/^(\s?\.?[a-zA-Z]+)+$/)) {
            this.setState({
                nameError: 'Enter a valid name'
            });
        } else {
            this.setState({
                nameError: ''
            });
        }
    }

    handleEmailChange = (event) => {
        let email = event.target.value;
        this.setState({email});

        email = email.trim();
        let emailValidator=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //eslint-disable-line

        if(this.state.name === '') {
            this.setState({
                nameError: 'Name is required'
            })
        }

        if(email === '') {
            this.setState({
                emailError: 'Email is required'
            });
        } else if(!email.match(emailValidator)) {
            this.setState({
                emailError: 'Enter a valid email'
            });
        } else {
            this.setState({
                emailError: ''
            });
        }
    }

    handlePasswordChange = (event) => {
        let password = event.target.value;
        this.setState({password});

        let passwordValidator = /(?=^.{6,}$)(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^A-Za-z0-9]).*/;

        if(this.state.name === '') {
            this.setState({
                nameError: 'Name is required'
            })
        } 

        if(this.state.email === '') {
            this.setState({
                emailError: 'Email is required'
            })
        }

        if(password === '') {
            this.setState({
                passwordError: 'Password is required'
            });
        } else if(!password.match(passwordValidator)) {
            this.setState({
                passwordError: 'Enter a valid password'
            });
        } else {
            this.setState({
                passwordError: ''
            });
        }

    }

    handleConfirmPasswordChange = (event) => {
        let confirmPassword = event.target.value;
        this.setState({confirmPassword});

        if(this.state.name === '') {
            this.setState({
                nameError: 'Name is required'
            })
        } 

        if(this.state.email === '') {
            this.setState({
                emailError: 'Email is required'
            })
        }

        if(this.state.password === '') {
            this.setState({
                passwordError: 'Password is required',
                confirmPasswordError: 'Enter valid password then confirm'
            })
        } else if(this.state.passwordError === '' && confirmPassword === '') {
            this.setState({
                confirmPasswordError: 'Confirmation is required'
            });
        } else if(this.state.passwordError !== ''){
            this.setState({
                confirmPasswordError: 'Enter valid password then confirm'
            });
        }  else {
            this.setState({
                confirmPasswordError: ''
            });
        }
    }

    doesPasswordMatch = () => {
        if(this.state.password === this.state.confirmPassword) {
            this.setState({
                confirmPasswordError: ''
            });
            return true;
        }
        this.setState({
            confirmPasswordError: "Password doesn't match"
        })
        return false;
    }

    isFormDirty = () => {
        let name = this.state.name;
        let email = this.state.email;
        let password = this.state.password;
        let confirmPassword = this.state.confirmPassword;

        let nameError = this.state.nameError;
        let emailError = this.state.emailError;
        let passwordError = this.state.passwordError;
        let confirmPasswordError = this.state.confirmPasswordError;

        if(name !== '' && email !== '' && password !== '' && confirmPassword !== '') {
            return true;
        } 
        if(name === '' && email === '' && password === '' && confirmPassword === '' && nameError === '' && emailError === '' && passwordError === '' && confirmPasswordError === ''){
            this.setState({
                nameError: 'Name is required',
                emailError: 'Email is required',
                passwordError: 'Password is required',
                confirmPasswordError: 'Confirmation is required'
            });
            return false;
        } 
        return false;
    }

    validateForm = () => {
        if(this.isFormDirty()) {
            let nameError = this.state.nameError;
            let emailError = this.state.emailError;
            let passwordError = this.state.passwordError;
            let confirmPasswordError = this.state.confirmPasswordError;

            if(nameError === '' && emailError === '' && passwordError === '' && confirmPasswordError === '') {
                if(this.doesPasswordMatch()) {
                    return true;
                } 
            } 
        }
        return false;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if(this.validateForm()) {
            let signupData = {
                name: this.state.name.trim(),
                email: this.state.email.trim(),
                password: this.state.password
            }
            this.props.handleSignupData(signupData);
        } 
    }

    handleShowPassword = () => {
        this.setState({
            showPassword: !this.state.showPassword
        })
    }

    handleShowConfirmPassword = () => {
        this.setState({
            showConfirmPassword: !this.state.showConfirmPassword
        })
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit} >
                <h1 className="Heading">Signup</h1>
                <Form.Group className="mb-3">
                    <Form.Label>Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={this.state.name} 
                        onChange={this.handleNameChange}
                        style={this.state.nameError !== '' ? ErrorStyle : (this.state.name !== '' ? SuccessStyle : null )}/>
                    <Form.Text style={{color: "red"}}>{this.state.nameError}</Form.Text>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="text" 
                        value={this.state.email} 
                        onChange={this.handleEmailChange}
                        style={this.state.emailError !== '' ? ErrorStyle : (this.state.email !== '' ? SuccessStyle : null )}/>
                    <Form.Text style={{color: "red"}}>{this.state.emailError}</Form.Text>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <InputGroup className="mb-3">
                        <Form.Control 
                            type={this.state.showPassword ? "text" : "password"} 
                            value={this.state.password} 
                            onChange={this.handlePasswordChange}
                            style={this.state.passwordError !== '' ? ErrorStyle : (this.state.password !== '' ? SuccessStyle : null )}/>
                        <Button variant="secondary" id="button-addon2"  onClick={this.handleShowPassword}>
                            {this.state.showPassword ? <FaRegEyeSlash /> :  <FaRegEye />  }
                        </Button>
                    </InputGroup>
                    <div className="Col">
                        <Form.Text style={{color: "red"}}>{this.state.passwordError}</Form.Text>
                        <Form.Text>
                            ( Minimum six characters, at least one uppercase letter, one lowercase letter, one digit and one special character )
                        </Form.Text>
                    </div>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Confirm Password</Form.Label>
                    <InputGroup className="mb-3">
                        <Form.Control 
                            type={this.state.showConfirmPassword ? "text" : "password"} 
                            value={this.state.confirmPassword} 
                            onChange={this.handleConfirmPasswordChange}
                            style={this.state.confirmPasswordError !== '' ? ErrorStyle : (this.state.confirmPassword !== '' ? SuccessStyle : null )}/>
                        <Button variant="secondary" id="button-addon2"  onClick={this.handleShowConfirmPassword}>
                            {this.state.showConfirmPassword ? <FaRegEyeSlash /> :  <FaRegEye />  }
                        </Button>
                    </InputGroup>
                    <Form.Text style={{color: "red"}}>{this.state.confirmPasswordError}</Form.Text>
                </Form.Group>
                <Button type="submit">Submit</Button>
            </Form>
        )
    }
}

const ErrorStyle = {
    border: "1px solid red"
}

const SuccessStyle = {
    border: "1.5px solid lightgreen"
}

export default Signup
