import React,{ Component } from 'react';
import { BrowserRouter as Router , Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Spinner } from 'react-bootstrap';
import Signup from './components/Signup.js';
import Header from './components/layout/Header.js';
import SubmissionResponse from './components/SubmissionResponse.js';
import Login from './components/Login.js';
import './App.css';
import LoginResponse from './components/LoginResponse.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      displaySignup: true,
      displayLogin: true,
      signupError: "",
      loginError: "",
      submissionStatus: false,
      loginSubmission: false
    }
  }

  handleSignupData = async (signupData) => {
    try {
      this.setState({
        isLoading: true,
        displaySignup: false
      });

      let response = await fetch('https://signup-apis.herokuapp.com/user/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(signupData)
      });

      let responseData = await response.json();

      this.setState({
        isLoading: false,
        submissionStatus: true
      });

      if(!response.ok) {
        let responseErrorMsg = responseData.errors[0].message;
        throw Error(responseErrorMsg);
      } else {
        console.log("success");
      }

    } catch(err) {
      if(err.message === "users.email must be unique") {
        this.setState({
          signupError: "Email is already registered"
        })
      } else {
        this.setState({
          signupError: "Network error! Try after sometime"
        })
      }
    }
  }

  handleLoginData = async (loginData) => {
    try {
      localStorage.clear();
      this.setState({
        isLoading: true,
        displayLogin: false
      });

      let response = await fetch('https://signup-apis.herokuapp.com/user/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(loginData)
      });

      let responseData = await response.json();

      this.setState({
        isLoading: false,
        loginSubmission: true
      });

      if(!response.ok) {
        console.log(responseData);
        if(responseData.msg) {
          throw Error(responseData.msg);
        }
      } else {
        localStorage.setItem("accessToken",responseData.accessToken);
        localStorage.setItem("refreshToken",responseData.refreshToken);
      }

    } catch(err) {
      if(err.message === "Password Incorrect") {
        this.setState({
          loginError: "Password Incorrect"
        })
      } else if(err.message === "User doesn't exist") {
        this.setState({
          loginError: "User doesn't exist"
        })
      } else {
        this.setState({
          loginError: "Network error! Try after sometime"
        })
      }
    }
  }

  render() {
    return (
      <Router>  
        <Header />        
        <div className="App">
          <Route exact path="/" render= {props => (
              <React.Fragment>
                <Spinner animation="border" className={ this.state.isLoading ? "SpinVisible" : "SpinInvisible" }/>
                {
                  this.state.displayLogin && <Login handleLoginData={this.handleLoginData}/>
                }
                {
                  this.state.loginSubmission && <LoginResponse loginError={this.state.loginError}/>
                }
              </React.Fragment>
          )}></Route>
          <Route path='/signup' render= { props => (
            <React.Fragment>
            <Spinner animation="border" className={ this.state.isLoading ? "SpinVisible" : "SpinInvisible" }/>
            {
              this.state.displaySignup && <Signup handleSignupData={this.handleSignupData}/>
            }
            {
              this.state.submissionStatus && <SubmissionResponse signupError={this.state.signupError}/>
            }
            </React.Fragment>
          )}></Route>
        </div>
      </Router>
    );
  }
}

export default App;
